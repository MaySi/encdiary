#!/usr/bin/python
import sys
import os
import time
import datetime
from simplecrypt import encrypt, decrypt, DecryptionException
from getpass import getpass
from personalPass import imp_pass

today = str(datetime.date.today().strftime("%d_%m_%Y"))
timestamp =  str(datetime.datetime.now().strftime("%d-%m-%Y %I:%M %p")).split(".")[0]
file_loc = os.path.abspath(os.path.dirname(__file__))

os.chdir(file_loc)
data_path = os.path.join(file_loc,"DIARY_FILES")
temptxt_path = os.path.join(file_loc,"TEMPTXT_FILES")

if not os.path.exists(data_path):
    os.system("mkdir {}".format("DIARY_FILES"))
if not os.path.exists(temptxt_path):
    os.system("mkdir {}".format("TEMPTXT_FILES"))
os.chdir(data_path)

# passwd = getpass()
passwd = imp_pass
def createNew(fi):
    try:
        decAndSave(fi)
        with open(os.path.join(temptxt_path,'{}.txt'.format(fi)), 'a') as df:
            # df.write(timestamp)
            print("file exists ::: successfully decrypted")
    except DecryptionException:
        print("Invalid password")
        # os.system("del {}.txt".format(fi))
        sys.exit()
    except:
        with open(os.path.join(temptxt_path,'{}.txt'.format(fi)), 'a') as sf:
            sf.write(timestamp)
            print("created new file")

def openForEdit(fi):
    os.system("notepad "+os.path.join(temptxt_path,'{}.txt'.format(fi)))

def encAndSave(fi):
    with open(os.path.join(temptxt_path,'{}.txt'.format(fi)), 'r') as sf, open('{}.enc'.format(fi), 'wb+') as df:
        df.write(encrypt(passwd, sf.read()))

def decAndSave(fi):
    with open('{}.enc'.format(fi), 'rb') as sf:
        secret = sf.read()
    message = decrypt(passwd, secret).decode()
    with open(os.path.join(temptxt_path,'{}.txt'.format(fi)), 'w') as df:
        df.write(message)




if(len(sys.argv) > 1):
    try:
        decAndSave(sys.argv[1])
        f = sys.argv[1]
    except DecryptionException as e:
        print("Invalid password")
        sys.exit()
    except:
        print("File Does Not Exist")
        ans = input("Create New? (y/n) ")
        if ans=="y":
            createNew(sys.argv[1])
            # f=today
            f = sys.argv[1]
        else:
            sys.exit()
else:
    createNew(today)
    f = today

print("Decrypting File...{}".format(f))
openForEdit(f)
print("Encrypting File...{}".format(f))
encAndSave(f)
os.system("del " + os.path.join(temptxt_path,'{}.txt'.format(f)))
# print(os.path.join(temptxt_path,'{}.txt'.format(f)))
