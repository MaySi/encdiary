#!/usr/bin/python
import sys
import os
import time
import datetime
from simplecrypt import encrypt, decrypt, DecryptionException
from getpass import getpass

file_loc = os.path.abspath(os.path.dirname(__file__))

os.chdir(file_loc)
data_path = os.path.join(file_loc,"ENC_FILES")

if not os.path.exists(data_path):
    os.system("mkdir {}".format("ENC_FILES"))
os.chdir(data_path)

passwd = getpass()

def encAndSave(fi):
    print("enc started")
    with open(os.path.join(file_loc,'{}'.format(fi)), 'rb') as sf, open('{}.enc'.format(fi), 'wb+') as df:
        df.write(encrypt(passwd, sf.read()))
    print("enc finished")




if(len(sys.argv)==2):
    try:
        encAndSave(sys.argv[1])
    except:
        print("File Does Not Exist")
        sys.exit()
else:
    print("wrong call")
