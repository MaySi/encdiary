#!/usr/bin/python
import sys
import os
import time
import datetime
from simplecrypt import encrypt, decrypt, DecryptionException
from getpass import getpass

file_loc = os.path.abspath(os.path.dirname(__file__))

os.chdir(file_loc)
data_path = os.path.join(file_loc,"DEC_FILES")

if not os.path.exists(data_path):
    os.system("mkdir {}".format("DEC_FILES"))
os.chdir(data_path)

passwd = getpass()

def decAndSave(fi):
    print("dec started")
    with open(os.path.join(file_loc,'{}'.format(fi)), 'rb') as sf:
        secret = sf.read()
    message = decrypt(passwd, secret)
    fi=fi.split(".")
    fi=fi[:-1]
    fi=".".join(fi)
    with open('{}'.format(fi), 'wb') as df:
        df.write(message)
    print("dec finished")




if(len(sys.argv)==2):
    try:
        decAndSave(sys.argv[1])
    except DecryptionException as e:
        print("Invalid password")
        sys.exit()
    except:
        print("File Does Not Exist")
        sys.exit()
else:
    print("wrong call")
